package com.github.xabgesagtx.fatlining.service.validation;

import com.github.xabgesagtx.fatlining.service.dtos.NewUserDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NewUserDTOValidatorTest {
	
	@Mock
	private ValidationUtils utils;
	
	@Mock
	private Errors errors;
	
	@InjectMocks
	private NewUserDTOValidator validator;

	@Test
	public void testValidate() {
		String name = "1";
		String password = "2";
		String repeatedPassword = "3";
		String email = "4";
		BigDecimal goalInKg = BigDecimal.ONE;
		NewUserDTO dto = NewUserDTO.of(name, password, repeatedPassword, email, goalInKg);
		validator.validate(dto, errors);
		verify(utils).validateName(name, errors);
		verify(utils).validateEmail(email, null, errors);
		verify(utils).validatePasswords(password, repeatedPassword, errors);
		verify(utils).validateGoalInKg(goalInKg, errors);
	}

}
