package com.github.xabgesagtx.fatlining.service.validation;

import com.github.xabgesagtx.fatlining.service.dtos.UserDetailsDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsDTOValidatorTest {

	@Mock
	private ValidationUtils utils;

	@Mock
	private Errors errors;
	
	@InjectMocks
	private UserDetailsDTOValidator validator;

	@Test
	public void testValidate() {
		Long id = 1L;
		String name = "2";
		String email = "3";
		BigDecimal goalInKg = BigDecimal.ONE;
		UserDetailsDTO dto = UserDetailsDTO.of(id, name, email, goalInKg);
		validator.validate(dto, errors);
		verify(utils).validateEmail(email, id, errors);
		verify(utils).validateGoalInKg(goalInKg, errors);
	}

}
