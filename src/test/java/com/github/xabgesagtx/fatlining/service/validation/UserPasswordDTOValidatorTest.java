package com.github.xabgesagtx.fatlining.service.validation;

import com.github.xabgesagtx.fatlining.service.dtos.UserPasswordDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserPasswordDTOValidatorTest {

	@Mock
	private ValidationUtils utils;

	@Mock
	private Errors errors;
	
	@InjectMocks
	private UserPasswordDTOValidator validator;

	@Test
	public void testValidate() {
		String currentPassword = "1";
		String password = "2";
		String repeatedPassword = "3";
		UserPasswordDTO dto = UserPasswordDTO.of(currentPassword, password, repeatedPassword);
		validator.validate(dto, errors);
		verify(utils).validateCurrentPassword(currentPassword, errors);
		verify(utils).validatePasswords(password, repeatedPassword, errors);
	}

}
