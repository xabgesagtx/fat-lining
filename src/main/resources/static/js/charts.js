const defaultDays = 30;

function setDaysInCookie(days) {
	Cookies.set('days', days, {expires : 90});
}

function setActiveButton(days) {
	$('#charts-navigation button').each(function() {
		let daysButton = $(this).data('days');
		if (daysButton === days) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});
}

function getCharts(days) {
	$.get(`/measurements/charts?days=${days}`, function (data) {
		if (data.valid) {
			setDaysInCookie(days);
			setActiveButton(days);
			let markings = [];
			if (data.goal) {
				markings = [{color: '#F00', lineWidth: 2, yaxis: {from: data.goal, to: data.goal}}];
			}
			let values;
			if (data.withTrend) {
				values = [data.measurements, data.trend];
			} else {
				values = [data.measurements];
			}
			$.plot($('#placeholder'), values, {
				xaxis: {
					mode: 'time'
				},
				yaxis: {
					min: data.minValue,
					max: data.maxValue
				},
				grid: {
					markings: markings
				}
			});
		}
	});
}

function getDaysFromCookie() {
	let days = Cookies.get('days');
	if (days) {
		return parseInt(days);
	} else {
		return defaultDays;
	}
}


$(document).ready(function(){
	$('#charts-navigation button').click(function() {
		let days = $(this).data('days');
		getCharts(days);
	});
	getCharts(getDaysFromCookie());
});