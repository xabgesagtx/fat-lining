package com.github.xabgesagtx.fatlining.service;

import com.github.xabgesagtx.fatlining.config.PaginationConfig;
import com.github.xabgesagtx.fatlining.model.WeightMeasurement;
import com.github.xabgesagtx.fatlining.persistence.WeightMeasurementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * 
 * Manager of all measurements related tasks
 *
 */
@Component
@RequiredArgsConstructor
public class WeightMeasurementManager {
	
	private final WeightMeasurementRepository repo;
	private final PaginationConfig paginationConfig;

	/**
	 * Find all for current user
	 * @return all measurments
	 */
	public Iterable<WeightMeasurement> findAllForUser() {
		return repo.findAllForUser(Sort.by(new Order(Direction.ASC, "dateTime")));
	}
	
	/**
	 * Find all for current user of the last days
	 * @return all measurments
	 */
	public Iterable<WeightMeasurement> findAllForUserAndDays(int days) {
		return repo.findAllForUserAndDays(LocalDate.now().minusDays(days).atStartOfDay(), Sort.by(new Order(Direction.ASC, "dateTime")));
	}
	
	/**
	 * Find a page of measurements sorted by the time of creation descending
	 * @param pageNumber index of the page
	 * @return page of measurements
	 */
	public Page<WeightMeasurement> findAsPageDesc(int pageNumber) {
		return repo.findAllPagedForUser(PageRequest.of(pageNumber, paginationConfig.getPageSize(), Sort.by(new Order(Direction.DESC, "dateTime"))));
	}
	
	/**
	 * Delete single measurement by id. Only admin or user who created that measurement can do that
	 * @param id of measurement
	 * @return true if measurment could be found, false otherwise
	 */
	public boolean delete(long id) {
		return repo.findById(id).map(wm -> {
			repo.delete(wm);
			return true;
		}).orElse(false);
	}
	
	/**
	 * Save a measurement
	 * @param wm to be saved
	 * @return saved measurement
	 */
	public WeightMeasurement save(WeightMeasurement wm) {
		return repo.save(wm);
	}
	
}
