package com.github.xabgesagtx.fatlining.service.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.github.xabgesagtx.fatlining.service.dtos.UserDetailsDTO;

/**
 * 
 * Validate user details change
 *
 */
@Component
@RequiredArgsConstructor
public class UserDetailsDTOValidator implements Validator {
	
	private final ValidationUtils utils;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(UserDetailsDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UserDetailsDTO dto = (UserDetailsDTO) target;
		utils.validateEmail(dto.getEmail(), dto.getId(), errors);
		utils.validateGoalInKg(dto.getGoalInKg(), errors);
	}

}
