package com.github.xabgesagtx.fatlining.persistence;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.github.xabgesagtx.fatlining.model.AppUser;
import com.github.xabgesagtx.fatlining.security.AppUserDetails;

import java.util.Optional;

/**
 * 
 * Service that provides the current auditor as {@link AppUser}
 *
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<AppUser> {

	@Override
	public Optional<AppUser> getCurrentAuditor() {
		Optional<AppUser> result = Optional.empty();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.isAuthenticated()) {
			result = Optional.of(((AppUserDetails) authentication.getPrincipal()).getUser());
		}
		return result;
	}

}
