package com.github.xabgesagtx.fatlining.web;

import com.github.xabgesagtx.fatlining.messages.Messages;
import com.github.xabgesagtx.fatlining.messages.MessagesService;
import com.github.xabgesagtx.fatlining.model.AppUser;
import com.github.xabgesagtx.fatlining.service.FatLiningConstants;
import com.github.xabgesagtx.fatlining.service.UsersManager;
import com.github.xabgesagtx.fatlining.service.dtos.NewUserDTO;
import com.github.xabgesagtx.fatlining.service.dtos.UserDetailsDTO;
import com.github.xabgesagtx.fatlining.service.dtos.UserPasswordDTO;
import com.github.xabgesagtx.fatlining.service.validation.NewUserDTOValidator;
import com.github.xabgesagtx.fatlining.service.validation.UserDetailsDTOValidator;
import com.github.xabgesagtx.fatlining.service.validation.UserPasswordDTOValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * 
 * Admin section of the website
 *
 */
@Controller
@RequestMapping("admin")
@RequiredArgsConstructor
public class AdminController {

	private final UsersManager manager;
	private final MessagesService messages;
	private final NewUserDTOValidator newUserDTOValidator;
	private final UserPasswordDTOValidator userPasswordDTOValidator;
	private final UserDetailsDTOValidator userDetailsDTOValidator;
	
	@InitBinder(FatLiningConstants.FIELD_NEW_USER)
	public void initBinderNewUser(WebDataBinder binder) {
		binder.addValidators(newUserDTOValidator);
	}
	
	@InitBinder(FatLiningConstants.FIELD_PASSWORD_DTO)
	public void initBinderUserPassword(WebDataBinder binder) {
		binder.addValidators(userPasswordDTOValidator);
	}
	
	@InitBinder(FatLiningConstants.FIELD_USER_DETAILS)
	public void initBinderUserDetails(WebDataBinder binder) {
		binder.addValidators(userDetailsDTOValidator);
	}

	@GetMapping("users")
	public String users(Model model) {
		model.addAttribute(FatLiningConstants.FIELD_USERS, manager.findAll());
		return FatLiningConstants.VIEW_USERS;
	}
	
	@GetMapping("users/{id}/details")
	public String details(@PathVariable("id") Long id, Model model) {
		model.addAttribute(FatLiningConstants.FIELD_USER_DETAILS, manager.findById(id).map(user -> UserDetailsDTO.of(user.getId(), user.getName(), user.getEmail(), user.getGraphConfig().getGoalInKg())).orElseThrow(() -> new ResourceNotFoundException(messages.getMessage(Messages.USERS_ERROR_NOT_FOUND, id.toString()))));
		model.addAttribute(FatLiningConstants.FIELD_URL, "/admin/users/" + id + "/details");
		return FatLiningConstants.VIEW_DETAILS; 
	}
	
	@PostMapping("users/{id}/details")
	public String detailsPost(@PathVariable("id") Long id, @Valid @ModelAttribute(FatLiningConstants.FIELD_USER_DETAILS) UserDetailsDTO userDetails, BindingResult bindingResult, Model model, RedirectAttributes redirectAttr) {
		AppUser user = findUser(id);
		if (!id.equals(userDetails.getId())) {
			throw new ManipulatedFormException(messages.getMessage(Messages.VALIDATION_ERROR_ID));
		} else if (bindingResult.hasErrors()) {
			model.addAttribute(FatLiningConstants.FIELD_URL, "/admin/users/" + id + "/details");
			return FatLiningConstants.VIEW_DETAILS;
		} else {
			manager.update(user, userDetails);
			redirectAttr.addFlashAttribute(FatLiningConstants.FIELD_SUCCESS, messages.getMessage(Messages.DETAILS_ADMIN_SUCCESS, user.getName()));
			return "redirect:/admin/users"; 
		}
	}

	@GetMapping("users/{id}/password")
	public String password(@PathVariable("id") Long id, Model model, @ModelAttribute(FatLiningConstants.FIELD_PASSWORD_DTO) UserPasswordDTO passwordDto) {
		findUser(id);
		model.addAttribute(FatLiningConstants.FIELD_URL, "/admin/users/" + id + "/password");
		return FatLiningConstants.VIEW_PASSWORD;
	}

	@PostMapping("users/{id}/password")
	public String passwordPost(@PathVariable("id") Long id, @Valid @ModelAttribute(FatLiningConstants.FIELD_PASSWORD_DTO) UserPasswordDTO dto, BindingResult bindingResult, Model model, RedirectAttributes redirectAttr) {
		AppUser user = findUser(id);
		if (bindingResult.hasErrors()) {
			model.addAttribute(FatLiningConstants.FIELD_URL, "/admin/users/" + id + "/password");
			return FatLiningConstants.VIEW_PASSWORD;
		} else {
			manager.changePassword(user, dto.getPassword());
			redirectAttr.addFlashAttribute(FatLiningConstants.FIELD_SUCCESS, messages.getMessage(Messages.PASSWORD_SUCCESS));
			return "redirect:/admin/users";
		}
	}

	@PostMapping("users/{id}/activate")
	public String activate(@PathVariable("id") Long id, RedirectAttributes redirectAttr) {
		AppUser user = findUser(id);
		user.setActivated(true);
		manager.save(user);
		redirectAttr.addFlashAttribute(FatLiningConstants.FIELD_SUCCESS, messages.getMessage(Messages.USERS_SUCCESS_ACTIVATE, user.getName()));
		return "redirect:/admin/users";
	}

	@PostMapping("users/{id}/deactivate")
	public String deactivate(@PathVariable("id") Long id, RedirectAttributes redirectAttr) {
		AppUser user = findUser(id);
		user.setActivated(false);
		manager.save(user);
		redirectAttr.addFlashAttribute(FatLiningConstants.FIELD_SUCCESS, messages.getMessage(Messages.USERS_SUCCESS_DEACTIVATE, user.getName()));
		return "redirect:/admin/users";
	}

	@GetMapping("users/new")
	public String newUser(@ModelAttribute(FatLiningConstants.FIELD_NEW_USER) NewUserDTO user) {
		return FatLiningConstants.VIEW_NEW_USER;
	}

	@PostMapping("users/new")
	public String newUserPost(@Valid @ModelAttribute(FatLiningConstants.FIELD_NEW_USER) NewUserDTO user, BindingResult bindingResult, RedirectAttributes redirectAttr) {
		if (bindingResult.hasErrors()) {
			return FatLiningConstants.VIEW_NEW_USER;
		} else {
			manager.create(user);
			redirectAttr.addFlashAttribute(FatLiningConstants.FIELD_SUCCESS, messages.getMessage(Messages.NEW_USER_SUCCESS, user.getName()));
			return "redirect:/admin/users";
		}
	}

	private AppUser findUser(Long id) {
		return manager.findById(id).orElseThrow(() -> new ResourceNotFoundException(messages.getMessage(Messages.USERS_ERROR_NOT_FOUND, id.toString())));
	}
}
