FROM openjdk:11.0.5-jre
ENV JAVA_OPTS=-Xmx128M
EXPOSE 8080
ADD /build/libs/fat-lining.jar fat-lining.jar
ENTRYPOINT ["/bin/sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar fat-lining.jar"]
